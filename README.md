# The Delta Project

This is the Delta Project, aiming to improve software reliability.

The main tool for this is the integration of massive assertion support into the programming language;
assertions can state properties such as
memory and CPU usage limits,
type constraints,
absence of information leakage,
etc.

Theory says that assertion checking can be made almost fully automatic,
only one kind of help needed is loop invariants and their equivalents.  
In practice, people will want to provide more assertions,
e.g. to state the properties of internal APIs.

Theory also warns that such an assertion checker has an exponential worst-case complexity;
practice from Hindley-Milner type inference indicates
that people avoid writing code that runs into this kind of problem.

Building the language is just the first step.
See [the long-term plan](docs/long-term-plan.md).


## Releases

There are currently no official releases;
these will come when non-contributors start taking interest.

There is currently no release regime; the project is just evolving.  
This will most likely change in the future.


## Downloads

To get the code and everything else, either clone the repository, or use the "Download" button above.

Everything is in one repository:
- [sources](src/),
- [build helper tools](build/),
- [documentation](docs/),
- [project organization tools and configuration](org/).


## Reporting issues

Use the "Issues" link in the left bar.


## Contributing

1. Clone the repository to your local machine,
1. make a branch,
1. hack on the code,
1. push back,
1. make a pull request on the web site,
1. follow the PR discussion until the PR is merged, superseded, or retracted.


## Etiquette

Be polite or be gone.

Criticize work, not people.  
(Accepting a critique of one's work can be hard enough.)

Actually, if you dislike something, do not criticize at all.  
Instead, provide actionable(!) suggestions for improving it.

Don't spam.  
If you do not have an actionable suggestion, stay silent.
