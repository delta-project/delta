Setting up Eclipse
==================

Basic install
-------------

Download the Java or the Java EE build for Java.  
(I think the Gradle Buildship plugin comes preinstalled with these.
If not, install it.)


Basic configuration
-------------------

Start Eclipse.  
When asked for a workspace,
navigate to the one that you cloned the `delta` repository into.  
`File` -> `Import` -> `Gradle` -> `Gradle Project`
(Do not "Import existing project",
this will fail due to a missing `.project` file.)  
`Window` -> `Show View` -> `Navigator`  
Double-click on `.classpath`. You'll get a text editor.  
Insert anywhere between `<classpath>` and `</classpath>`:  
```
	<classpathentry exported="true" kind="con" path="org.eclipse.buildship.core.gradleclasspathcontainer"/>
```

Go back to `Project Explorer`. You should see various directories,
a line stating the `JRE System Library`,
and a line saying `Project and External Dependencies`.


Recommended plugins
-------------------

Infinitest


Required settings
-----------------

`Window` -> `Preferences` -> `Java` -> `Code Style` -> `Formatter`  
Click on `Import...`.  
Navigate to the project directory, then to `org/eclipse`,
and select `java-source-formatting.xml`.  
This should switch you to `Java Conventions [spaces, wrap where necessary]`,
which pretty much describes the coding style for this project.
