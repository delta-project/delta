`penrose-triangle.svg` contains the project logo.

`make-logo-files.sh` is a script that generates various logo files,
ready to use.  
After generation, the generated files need to be uploaded to their respective
locations.
