#!/bin/sh

# This script generates all logo files that may be needed on various occasions.
#
# It assumes that penrose-triangle.svg is in the same directory as the script,
# and will generate the files into the same directory.
# The generated files are on .gitignore so they won't be accidentally added to
# the respository - if anything needs to be changed, change either this script
# or penrose-triangle.svg.

# Where this script lives - this is also the output directory
DIR=$(dirname $0)
# File to use
input_file="${DIR}/penrose-triangle.svg"
# Maximum of width and height of the input file
input_size="163"

# Code

construct_read_options() {
  # Construct a string of ImageMagick command-line options that will make it
  # read a vector file at a given pixel size.
  local output_size
  output_size=${1}
  # global value to return the read options in
  read_options=""

  # ImageMagick convert setting: Transparent background
  local background
  read_options="${read_options} -background none"
  # ImageMagick convert setting: density (DPI to assume when reading)
  # We need to tell IM an assumed DPI so that it renders the vector image at
  # the desired size.
  # As an added complication, shell arithmetic is integer-only so we need to
  # use bc, and tell it to actually retain fractions during division.
  local input_dpi
  # http://www.imagemagick.org/script/formats.php says the default is 90.
  input_dpi="90"
  local density_value
  density_value=$(echo "scale=5; ${input_dpi} * ${output_size} / ${input_size}" | bc)
  read_options="${read_options} -density ${density_value}"
  # ImageMagick convert setting: canvas size to render into
  read_options="${read_options} -resize ${output_size}x${output_size}"
  # ImageMagick convert setting: maximum number of colors
  # This is applied to help compression.
  read_options="${read_options} -colors 256"
  # ImageMagick convert setting: quality
  # For PNG and ICO, this is two digits: CT
  # C = compression level 1-9
  # T = PNG filter type, 0 none, 1-9 various compression helper filters
  # Experiments have determined that using no filter gives the smallest file :-)
  read_options="${read_options} -quality 90"
  # ImageMagick convert parameter: input file
  read_options="${read_options} $input_file"

}

# Not all logos are currently in use, we just generate all that we know how.

# Logo for GitHub
# https://help.github.com/articles/how-do-i-set-up-my-profile-picture/
# says that the image should be roughly 500x500 pixels.
construct_read_options 500
convert $read_options ${DIR}/github-logo.png

# Logo for GitLab
# GitLab does not currently have any recommendation except that the file should
# not be larger than 200 KB.
# The largest display size seems to be 160x160, so that's what we're scaling to.
# GitLab currently uses a circular mask (actually a CSS styling with rounded
# corners that gives that circular mask effect). To fit into that circle, the
# logo would need to be scaled to 141 pixels and leave a distance of 3 or 4
# pixels at the top. As of 2015-09-30, it seems that GitLab will remove that
# styling, so the logo probably does not need to be adapted.
construct_read_options 160
convert $read_options ${DIR}/gitlab-logo.png

# Favicon for https://delta-project.org
# https://en.wikipedia.org/wiki/Favicon#File_format_support
# says that browser .ico files can have a size of 16, 32, 48, or 64 pixels.
construct_read_options 16
read_options_16=${read_options}
construct_read_options 32
read_options_32=${read_options}
construct_read_options 48
read_options_48=${read_options}
construct_read_options 64
read_options_64=${read_options}
convert \
  \( ${read_options_16} \) \
  \( ${read_options_32} \) \
  \( ${read_options_48} \) \
  \( ${read_options_64} \) \
  favicon.ico

exit

convert \
  $background \
  $density \
  $resize \
  inkscape:penrose-triangle.svg \
  penrose-triangle.png
#  \( -clone 0 -resize 16x16 \) \
#  \( -clone 0 -resize 32x32 \) \
#  \( -clone 0 -resize 48x48 \) \
#  \( -clone 0 -resize 64x64 \) \
#  -delete 0 \
#  -colors 256 \
#  favicon.ico
