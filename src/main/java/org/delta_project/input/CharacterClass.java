package org.delta_project.input;

public enum CharacterClass {

    /**
     * Character is invalid, i.e. must not appear in any program source code.
     */
    INVALID,

    /**
     * Character is part of a line break.<br>
     * This is based on the "Mandatory breaks" rules (LB4 to LB12) from Unicode
     * TR#7, applied as follows:
     * <ul>
     * <li>This implementation serves just the core language, which is
     * restricted to 7-bit ASCII. So we ignore characters outside the 7-bit
     * ASCII range.
     * <li>LB4 (always break after hard line breaks) means we classify the BK
     * characters as line breaks.<br>
     * These are FORM FEED, LINE TABULATION(VT), LINE SEPARATOR, and PARAGRAPH
     * SEPARATOR.<br>
     * FORM FEED is occasionally used by text editors, so we accept it as a line
     * break.<br>
     * LB4 explicitly allows to opting out of LINE TABULATION (VT) support, and
     * since VT is very, very obsolete, we indeed do not support it.<br>
     * LINE SEPARATOR and PARAGRAPH SEPARATOR are outside the 7-bit ASCII range,
     * so we ignore these as well.
     * <li>LB5 (treat CR followed by LF, as well as CR, LF, and NL as hard line
     * breaks) means we classify CR and LF as line breakers.<br>
     * NL is outside the 7-bit ASCII range, hence ignored.
     * <li>LB6 (do not break before hard line breaks) ignored because it does
     * not classify any character.<br>
     * Also, in programming, we do generally break before hard line breaks, this
     * generates an empty line.
     * <li>LB7 (do not break before spaces or zero width space) is ignored in
     * programming: spaces are considered normal line content.
     * <li>LB8 (break before any character following a zero-width space, even if
     * one or more spaces intervene),<br>
     * LB9 (do not break a combining character sequence; treat it as if it has
     * the line breaking class of the base character in all of the following
     * rules),<br>
     * LB10 (treat any remaining combining mark as AL),<br>
     * LB11 (do not break before or after Word joiner and related characters),
     * and<br>
     * LB12 (do not break after NBSP and related characters)<br>
     * are all ignored because they all refer to situations where at least one
     * character is outside the 7-bit ASCII range.
     * <li>
     * </ul>
     */
    LINE_BREAK,

    /**
     * Character is empty space.
     * <p>
     * N.B. the Tab character (code 9) is intentionally not allowed.<br>
     * Having two empty space characters, with different display widths which
     * would be implementation-defined for the Tab character, would just confuse
     * users and compiler writers.<br>
     * Today, we have smart-indenting editors and don't need Tab characters in
     * the source code anymore.
     */
    SPACE,

    /**
     * Character is a punctuation character.<br>
     * Punctuation characters are always a single-character token.
     */
    PUNCTUATION,

    /**
     * Character is a quote character.<br>
     * Quote characters delimit string literals.
     */
    QUOTE,

    /**
     * Character is a digit.<br>
     * Digits appear in numbers and names.
     */
    DIGIT,

    /**
     * Character is a letter.<br>
     * Letters appear in names.
     */
    LETTER,

    /**
     * Character is an operator.<br>
     * Operator characters appear in names.
     */
    OPERATOR;

    // //////////////////////////////////////////////////////////////////////
    // Set up and use charClasses

    /**
     * charClasses Keeps track of the character class of each 7-bit ASCII
     * character.
     */
    private static final CharacterClass[] charClasses =
            new CharacterClass[128];

    static {
        for (int i = 0; i < charClasses.length; i++) {
            charClasses[i] = INVALID;
        }
    }

    /**
     * Helper function to set a character class.
     * <p>
     * Throws an exception if a character is assigned two character classes;
     * that would be a bug in the initialization code for charClasses.
     */
    private static void setClass(char c, CharacterClass charClass) {
        if (charClasses[c] != INVALID) {
            throw new RuntimeException(
                    String.format(
                            "Character '%c' has two character classes, "
                                    + "%s and %s",
                            (int) c, charClasses[c], charClass));
        }
        charClasses[c] = charClass;
    }

    private static void setClass(String s, CharacterClass charClass) {
        for (char c : s.toCharArray()) {
            setClass(c, charClass);
        }
    }

    private static void setClass(
            char from, char to, CharacterClass charClass)
    {
        for (char c = from; c <= to; c++) {
            setClass(c, charClass);
        }
    }

    public static final char LINE_FEED = 0x000a;
    public static final char FORM_FEED = 0x000c;
    public static final char CARRIAGE_RETURN = 0x000d;

    static {
        setClass(LINE_FEED, LINE_BREAK);
        setClass(FORM_FEED, LINE_BREAK);
        setClass(CARRIAGE_RETURN, LINE_BREAK);

        setClass(' ', SPACE);

        setClass("(),.:;?[]{}", PUNCTUATION);

        setClass("\"'`", QUOTE);

        setClass('0', '9', DIGIT);

        setClass('A', 'Z', LETTER);
        setClass('a', 'z', LETTER);
        setClass("$_", LETTER);

        setClass("+-*/<=>!#%&@\\^|~", OPERATOR);

        for (char c = 32; c < 127; c++) {
            if (getCharacterClass(c) == INVALID) {
                throw new RuntimeException(String.format(
                        "Character '%c' did not get a classification", c));
            }
        }
    }

    // //////////////////////////////////////////////////////////////////////
    // Public API

    /**
     * The character class of @{code c}.
     */
    public static CharacterClass getCharacterClass(char c) {
        if (c > 128) {
            return INVALID;
        }
        return charClasses[c];
    }

}
