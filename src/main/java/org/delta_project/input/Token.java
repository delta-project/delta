package org.delta_project.input;

public class Token {

    private final int lineNr;
    private final int columnNr;
    private final String text;
    private final TokenType type;

    public Token(int lineNr, int columnNr, String text, TokenType type) {
        super();
        this.lineNr = lineNr;
        this.columnNr = columnNr;
        this.text = text;
        this.type = type;
    }

    public int getLineNr() {
        return lineNr;
    }

    public int getColumnNr() {
        return columnNr;
    }

    public String getText() {
        return text;
    }

    public TokenType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + columnNr;
        result = prime * result + lineNr;
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Token other = (Token) obj;
        if (columnNr != other.columnNr)
            return false;
        if (lineNr != other.lineNr)
            return false;
        if (text == null) {
            if (other.text != null)
                return false;
        } else if (!text.equals(other.text))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Token [lineNr=" + lineNr
                + ", columnNr=" + columnNr
                + ", text=" + text
                + ", type=" + type
                + "]";
    }

}
