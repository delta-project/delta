package org.delta_project.input;

import static org.delta_project.input.CharacterClass.CARRIAGE_RETURN;
import static org.delta_project.input.CharacterClass.DIGIT;
import static org.delta_project.input.CharacterClass.INVALID;
import static org.delta_project.input.CharacterClass.LETTER;
import static org.delta_project.input.CharacterClass.LINE_BREAK;
import static org.delta_project.input.CharacterClass.LINE_FEED;
import static org.delta_project.input.CharacterClass.OPERATOR;
import static org.delta_project.input.CharacterClass.SPACE;
import static org.delta_project.input.CharacterClass.getCharacterClass;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An {@link Iterator} over the tokens found in the text from a {@link Reader}.
 */
public class TokenIterator implements Iterator<Token> {

    // //////////////////////////////////////////////////////////////////////

    private Reader input;

    /**
     * Current line number.<br>
     * Always valid.
     */
    private int lineNr = 1;

    /**
     * Current column number.<br>
     * Always valid.
     */
    private int columnNr = 0;

    /**
     * The next character not yet returned as part of a {@code Token}.<br>
     * Invalid if {@code isBeforeStartOfInput} or {@code isAfterEndOfInput} is
     * {@code true}.
     */
    private char character;

    /**
     * Did we read anything yet?
     */
    private boolean isBeforeStartOfInput = true;

    /**
     * Are we past the end of the input?
     */
    private boolean isAfterEndOfInput = false;

    public TokenIterator(Reader input) {
        this.input = input;
    }

    // //////////////////////////////////////////////////////////////////////
    // Reading characters and updating state

    private void readOneCharacter() {
        int newCharacter;
        try {
            newCharacter = input.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (newCharacter == -1) {
            // We got an end-of-file
            columnNr++;
            isAfterEndOfInput = true;
        }
        columnNr++;
        character = (char) newCharacter;
    }

    /**
     * Record that it turned out that {@code character} is actually the first
     * character on a new line.
     */
    private void recordLineEnd() {
        lineNr++;
        columnNr = 1;
    }

    // //////////////////////////////////////////////////////////////////////
    // Iterator interface

    @Override
    public boolean hasNext() {
        if (isBeforeStartOfInput) {
            isBeforeStartOfInput = false;
            readOneCharacter();
        }
        return !isAfterEndOfInput;
    }

    @Override
    public Token next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        return getNextToken();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    // //////////////////////////////////////////////////////////////////////
    // Actual logic

    private Token getNextToken() {
        if (isBeforeStartOfInput) {
            readOneCharacter();
        }
        // Postcondition of readOneCharacter():
        assert !isBeforeStartOfInput;
        // Remaining precondition of getNextToken() to fulfil hasNext():
        assert !isAfterEndOfInput;
        // Record the beginning of the token.
        // We'll need that to construct the Token object at the end.
        int startingLineNr = lineNr;
        int startingColumnNr = columnNr;
        char startingCharacter = character;
        StringBuffer text = new StringBuffer(character);
        // Record the character class of the first character.
        // This decides the end-of-token condition.
        CharacterClass characterClass = getCharacterClass(character);
        TokenType tokenType;
        switch (characterClass) {
        case INVALID:
            tokenType = TokenType.INVALID;
            break;
        case SPACE:
            tokenType = TokenType.SPACE;
            break;
        case LINE_BREAK:
            tokenType = TokenType.LINE_BREAK;
            break;
        case DIGIT:
            tokenType = TokenType.INTEGER_LITERAL;
            break;
        case QUOTE:
            tokenType = TokenType.STRING_LITERAL;
            break;
        case LETTER:
        case OPERATOR:
        case PUNCTUATION:
            tokenType = TokenType.NAME_WORD;
            break;
        default:
            throw new RuntimeException("Can't happen: Invalid enum value");
        }
        while (!isAfterEndOfInput) {
            char lastCharacter = character;
            readOneCharacter();
            boolean foundEndOfToken;
            switch (characterClass) {
            case INVALID:
                foundEndOfToken = getCharacterClass(character) != INVALID;
                break;
            case LINE_BREAK:
                foundEndOfToken =
                        lastCharacter != CARRIAGE_RETURN
                                || character != LINE_FEED;
                if (foundEndOfToken) {
                    recordLineEnd();
                }
                break;
            case DIGIT:
                foundEndOfToken = getCharacterClass(character) != DIGIT;
                break;
            case LETTER:
                foundEndOfToken =
                        getCharacterClass(character) != LETTER
                                && getCharacterClass(character) != DIGIT;
                break;
            case OPERATOR:
                foundEndOfToken = getCharacterClass(character) != OPERATOR;
                break;
            case PUNCTUATION:
                foundEndOfToken = true;
                break;
            case QUOTE:
                foundEndOfToken =
                        lastCharacter == startingCharacter
                                || getCharacterClass(character) == LINE_BREAK;
                break;
            case SPACE:
                foundEndOfToken = getCharacterClass(character) != SPACE;
                break;
            default:
                throw new RuntimeException("Can't happen: Invalid enum value");
            }
            if (foundEndOfToken)
                break;
            readOneCharacter();
        }
        return new Token(
                startingLineNr, startingColumnNr, text.toString(), tokenType);
    }

}
