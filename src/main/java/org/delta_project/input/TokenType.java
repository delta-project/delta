package org.delta_project.input;

public enum TokenType {

    /** A sequence of invalid characters. */
    INVALID,

    /** Empty space. */
    SPACE,

    /** An end-of-line marker. */
    LINE_BREAK,

    /**
     * A sequence of digits.<br>
     * N.B.: We do not have floating-point literals.
     */
    INTEGER_LITERAL,

    /** A string literal. */
    STRING_LITERAL,

    /** A name word (part of a name). */
    NAME_WORD,

}
