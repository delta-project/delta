package org.delta_project.input;

import org.junit.Test;

public class CharacterClassTest {

    /**
     * Test that negative character values do not cause an
     * {@link IndexOutOfBoundsException} when looking up character classes in
     * {@link CharacterClass.charClasses}.
     */
    @Test
    public void testNegativeCharClass() {
        CharacterClass.getCharacterClass((char) -1);
    }

}
