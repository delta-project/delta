package org.delta_project.input;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.io.StringReader;

import org.junit.Test;

import com.google.common.collect.Lists;

public class TokenIteratorTest {

    private static TokenIterator createSource(String source) {
        return new TokenIterator(new StringReader(source));
    }

    @Test
    public void testEmptyInput() {
        TokenIterator tokenIterator = createSource("");
        assertThat(
                Lists.newArrayList(tokenIterator),
                equalTo(Lists.<Token>newArrayList()));
    }

}
