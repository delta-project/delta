/**
 * Not actually a test, just a quick hack to study the Unicode classification of
 * all 7-bit ASCII characters as defined by the JDK.
 */
package org.delta_project.input;

abstract class UnicodeAttr {
    public abstract String getLeaderText();

    public abstract String getValue(char c);
}

class TitleAttr extends UnicodeAttr {
    public String getLeaderText() {
        return "Character ";
    }

    public String getValue(char c) {
        return "  " + c;
    }
}

class TypeAttr extends UnicodeAttr {
    public String getLeaderText() {
        return "Type ";
    }

    public String getValue(char c) {
        return String.format(" %2d", Character.getType(c));
    }
}

class IsType extends UnicodeAttr {
    String leaderText;
    int type;

    public IsType(String leaderText, int type) {
        this.leaderText = leaderText;
        this.type = type;
    }

    public String getLeaderText() {
        return leaderText;
    }

    public String getValue(char c) {
        return Character.getType(c) == type ? "  x" : "   ";
    }
}

public class UnicodeCharacterClasses7Bit {

    public static void main(String[] args) {
        UnicodeAttr[] attrs = {
                new TitleAttr(),
                // new TypeAttr(),
                new IsType(
                        "SPACE_SEPARATOR",
                        Character.SPACE_SEPARATOR),
                new IsType(
                        "OTHER_PUNCTUATION",
                        Character.OTHER_PUNCTUATION),
                new IsType(
                        "CURRENCY_SYMBOL",
                        Character.CURRENCY_SYMBOL),
                new IsType(
                        "START_PUNCTUATION",
                        Character.START_PUNCTUATION),
                new IsType(
                        "END_PUNCTUATION",
                        Character.END_PUNCTUATION),
                new IsType(
                        "MATH_SYMBOL",
                        Character.MATH_SYMBOL),
                new IsType(
                        "DASH_PUNCTUATION",
                        Character.DASH_PUNCTUATION),
                new IsType(
                        "DECIMAL_DIGIT_NUMBER",
                        Character.DECIMAL_DIGIT_NUMBER),
                new IsType(
                        "UPPERCASE_LETTER",
                        Character.UPPERCASE_LETTER),
                new IsType(
                        "MODIFIER_SYMBOL",
                        Character.MODIFIER_SYMBOL),
                new IsType(
                        "LOWERCASE_LETTER",
                        Character.LOWERCASE_LETTER),
                new IsType(
                        "CONNECTOR_PUNCTUATION",
                        Character.CONNECTOR_PUNCTUATION),
        };
        char[][] ranges = {
                { 32, 48 },
                { 48, 64 },
                { 64, 80 },
                { 80, 96 },
                { 96, 112 },
                { 112, 127 },
        };
        int maxLeaderSize = 0;
        for (UnicodeAttr attr : attrs) {
            maxLeaderSize =
                    Math.max(maxLeaderSize, attr.getLeaderText().length());
        }
        for (char[] range : ranges) {
            for (UnicodeAttr attr : attrs) {
                String leaderText = attr.getLeaderText();
                System.out.print(leaderText);
                for (int i = leaderText.length(); i <= maxLeaderSize; i++) {
                    System.out.print(" ");
                }
                for (char c = range[0]; c < range[1]; c++) {
                    System.out.print(attr.getValue(c));
                }
                System.out.println();
            }
            System.out.println();
        }
    }

}
